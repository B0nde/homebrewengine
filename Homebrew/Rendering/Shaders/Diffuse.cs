﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Graphics.OpenGL4;

namespace Homebrew.Rendering.Shaders
{
    public class Diffuse : ShaderProgram
    {
        internal Diffuse()
        {
            var vertex = new Shader("330", ShaderType.VertexShader);
            var fragment = new Shader("330", ShaderType.FragmentShader);

            vertex.AddLayout(0, "vec3", "positions");
            vertex.AddLayout(1, "vec2", "texcoords");
            vertex.AddOutVariable("vec2", "v_texcoord");

            vertex.AddLine(@"
            void main()
            {
                v_texcoord = texcoords;
                gl_Position = vec4(positions, 1.0);
            }
            ");

            fragment.AddOutVariable("vec4", "o_color");
            fragment.AddInVariable("vec2", "v_texcoord");
            fragment.AddUniform("sampler2D", "tex0");

            fragment.AddLine(@"
            void main()
            {
                vec4 tColor = texture(tex0, v_texcoord);
                o_color = tColor;
            }
            ");

            AddShader(vertex);
            AddShader(fragment);

            LinkAndValidate();
        }
    }
}
