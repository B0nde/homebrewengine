﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew.Utilities;
using OpenTK;
using OpenTK.Graphics.OpenGL4;

namespace Homebrew.Rendering.Shaders
{
    public class ShaderProgram
    {
        private static int boundId = 0;

        private int programId;
        private Dictionary<string, int> uniformLocations;
        private List<string> uniforms; 

        public ShaderProgram()
        {
            uniformLocations = new Dictionary<string, int>();
            uniforms = new List<string>();
            programId = GL.CreateProgram();
        }

        public void AddShader(Shader shader)
        {
            var id = shader.Compile();
            GL.AttachShader(programId, id);
            GL.DeleteShader(id);
            uniforms.AddRange(shader.Uniforms);
            //Console.WriteLine(shader.Source);
        }

        public void LinkAndValidate()
        {
            uniformLocations.Clear();
            GL.LinkProgram(programId);

            if (!HandleError(GetProgramParameterName.LinkStatus))
            {
                GL.ValidateProgram(programId);

                if (!HandleError(GetProgramParameterName.ValidateStatus))
                {
                    for (int i = 0; i < uniforms.Count; i++)
                    {
                        var name = uniforms[i];
                        var location = GL.GetUniformLocation(programId, name);
                        uniformLocations.Add(name, location);
                    }

                    uniforms.Clear();
                }
            }
        }

        public void SetUniform(string name, Vector2 v)
        {
            var loc = uniformLocations[name];
            GL.Uniform2(loc, v);
        }

        public void SetUniform(string name, Vector4 v)
        {
            var loc = uniformLocations[name];
            GL.Uniform4(loc, v);
        }

        public void SetUniform(string name, Vector3 v)
        {
            var loc = uniformLocations[name];
            GL.Uniform3(loc, v);
        }

        public void SetUniform(string name, Matrix4 mat)
        {
            var loc = uniformLocations[name];
            GL.UniformMatrix4(loc, false, ref mat);
        }

        public void SetUniform(string name, int i)
        {
            var loc = uniformLocations[name];
            GL.Uniform1(loc, i);
        }

        public void SetUniform(string name, float f)
        {
            var loc = uniformLocations[name];
            GL.Uniform1(loc, f);
        }

        public void Bind()
        {
            if (boundId != programId)
            {
                boundId = programId;
                GL.UseProgram(programId);
            }
        }

        private bool HandleError(GetProgramParameterName param)
        {
            int status;
            GL.GetProgram(programId, param, out status);

            if (status != 1)
            {
                var info = GL.GetProgramInfoLog(programId);
                Logger.Instance.Log(LoggerLevel.Error, "Error in shaderprogram:\n" + info);
                return true;
            }

            return false;
        }
    }
}
