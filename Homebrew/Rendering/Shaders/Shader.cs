﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew.Utilities;
using OpenTK.Graphics.OpenGL4;

namespace Homebrew.Rendering.Shaders
{
    public class Shader
    {
        public string Source { get; set; }
        public List<string> Uniforms { get; private set; }

        private ShaderType type;

        public Shader(string version, ShaderType type)
        {
            this.type = type;

            Source = "";
            Uniforms = new List<string>();
            AddLine("#version " + version);
        }

        public void AddInVariable(string type, string name)
        {
            AddLine("in " + type + " " + name + ";");
        }

        public void AddOutVariable(string type, string name)
        {
            AddLine("out " + type + " " + name + ";");
        }

        public void AddUniform(string type, string name)
        {
            AddLine("uniform " + type + " " + name + ";");
            Uniforms.Add(name);
        }

        public void AddLayout(int location, string type, string name)
        {
            AddLine("layout (location = " + location + ") in " + type + " " + name + ";");
        }

        public void AddLine(string line)
        {
            Source += line + "\n";
        }

        public int Compile()
        {
            int id = GL.CreateShader(type);
            GL.ShaderSource(id, Source);
            GL.CompileShader(id);
            HandleError(id, ShaderParameter.CompileStatus);
            return id;
        }

        private static bool HandleError(int id, ShaderParameter check)
        {
            int status;
            GL.GetShader(id, check, out status);

            if (status != 1)
            {
                var info = GL.GetShaderInfoLog(id);
                Logger.Instance.Log(LoggerLevel.Error, "Error in shader:\n" + info);
                return false;
            }

            return true;
        }
    }
}
