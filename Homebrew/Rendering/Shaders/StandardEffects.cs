﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homebrew.Rendering.Shaders
{
    public class StandardEffects
    {
        private static StandardEffects instance;
        public static StandardEffects Instance { get { return instance ?? (instance = new StandardEffects()); } }

        private Dictionary<string, ShaderProgram> programs; 

        private StandardEffects()
        {
            programs = new Dictionary<string, ShaderProgram>();
            
            programs.Add("Diffuse", new Diffuse());
            programs.Add("SpriteDiffuse", new SpriteEffect());
        }

        public ShaderProgram GetEffect(string name)
        {
            return programs[name];
        }
    }
}
