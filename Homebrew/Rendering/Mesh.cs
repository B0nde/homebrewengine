﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL4;

namespace Homebrew.Rendering
{
    public class Mesh
    {
        public const int POSITION_LOCATION = 0;
        public const int TEXCOORD_LOCATION = 1;

        public static readonly Mesh PLANE = CreatePlane1X1();

        private static int boundId = 0;

        public int Size { get; private set; }

        private int vao;
        private int texcoordBuffer;
        private int positionBuffer;
        private List<float> positions;
        private List<float> texcoords; 

        public Mesh()
        {
            positions = new List<float>();
            texcoords = new List<float>();

            vao = GL.GenVertexArray();

            var ids = new int[2];
            GL.GenBuffers(ids.Length, ids);

            positionBuffer = ids[0];
            texcoordBuffer = ids[1];
        }

        public void Update()
        {
            Bind();

            GL.EnableVertexAttribArray(POSITION_LOCATION);
            GL.EnableVertexAttribArray(TEXCOORD_LOCATION);

            UpdateBuffer(positionBuffer, POSITION_LOCATION, 3, positions);
            UpdateBuffer(texcoordBuffer, TEXCOORD_LOCATION, 2, texcoords);

            Unbind();

            GL.DisableVertexAttribArray(TEXCOORD_LOCATION);
            GL.DisableVertexAttribArray(POSITION_LOCATION);

            Size = positions.Count;
        }

        public void Bind()
        {
            if (boundId != vao)
            {
                boundId = vao;
                GL.BindVertexArray(vao);
            }
        }

        public void AddVertex(Vector3 position, Vector2 texcoord)
        {
            positions.Add(position.X);
            positions.Add(position.Y);
            positions.Add(position.Z);

            texcoords.Add(texcoord.X);
            texcoords.Add(texcoord.Y);
        }

        public static void Unbind()
        {
            boundId = 0;
            GL.BindVertexArray(0);
        }

        private static void UpdateBuffer(int bufferId, int location, int numElements, List<float> data)
        {
            GL.BindBuffer(BufferTarget.ArrayBuffer, bufferId);
            GL.BufferData(BufferTarget.ArrayBuffer, new IntPtr(data.Count * sizeof(float)), data.ToArray(), BufferUsageHint.StaticDraw);
            GL.VertexAttribPointer(location, numElements, VertexAttribPointerType.Float, false, 0, 0);
        }

        public static Mesh CreatePlane(Vector3 offset, Vector2 size)
        {
            var hs = size/2f;

            var plane = new Mesh();

            plane = new Mesh();

            plane.AddVertex(new Vector3(offset.X - hs.X, offset.Y - hs.Y, 0f), new Vector2(0f, 1f));
            plane.AddVertex(new Vector3(offset.X - hs.X, offset.Y + hs.Y, 0f), new Vector2(0f, 0f));
            plane.AddVertex(new Vector3(offset.X + hs.X, offset.Y + hs.Y, 0f), new Vector2(1f, 0f));
            plane.AddVertex(new Vector3(offset.X + hs.X, offset.Y + hs.Y, 0f), new Vector2(1f, 0f));
            plane.AddVertex(new Vector3(offset.X + hs.X, offset.Y - hs.Y, 0f), new Vector2(1f, 1f));
            plane.AddVertex(new Vector3(offset.X - hs.X, offset.Y - hs.Y, 0f), new Vector2(0f, 1f));

            plane.Update();

            return plane;
        }

        private static Mesh CreatePlane1X1()
        {
            return CreatePlane(new Vector3(), new Vector2(1f, 1f));
        }
    }
}
