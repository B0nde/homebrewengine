﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rectangle = Homebrew.Entities.Components.Physics.Rectangle;

namespace Homebrew.Rendering
{
    public class Spritesheet : Texture
    {
        public Dictionary<string, Rectangle> Sprites { get; private set; } 

        public Spritesheet(Bitmap bitmap, Dictionary<string, Rectangle> sprites = null) : base(bitmap)
        {
            if(sprites != null)
                Sprites = sprites;
            else
                Sprites = new Dictionary<string, Rectangle>();
        }
    }
}
