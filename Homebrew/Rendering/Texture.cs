﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK.Graphics.OpenGL4;
using PixelFormat = OpenTK.Graphics.OpenGL4.PixelFormat;

namespace Homebrew.Rendering
{
    public class Texture
    {
        private static int boundId = 0;

        public int Width { get; private set; }
        public int Height { get; private set; }

        private int id;

        public Texture(Bitmap bitmap)
        {
            id = GL.GenTexture();

            Width = bitmap.Width;
            Height = bitmap.Height;

            var data = bitmap.LockBits(new Rectangle(0, 0, Width, Height), ImageLockMode.ReadOnly,
                System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            Bind();
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, Width, Height, 0, PixelFormat.Bgra, PixelType.UnsignedByte, data.Scan0);
            bitmap.UnlockBits(data);

            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);
        }

        public void Bind()
        {
            if (boundId != id)
            {
                boundId = id;
                GL.BindTexture(TextureTarget.Texture2D, id);
            }
        }
    }
}
