﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew.Entities;
using Homebrew.Entities.Components;

namespace Homebrew.GameStates
{
    public class GameState
    {
        private List<Entity> entities;
        private List<Entity> newEntities; 

        public GameState()
        {
            entities = new List<Entity>();
            newEntities = new List<Entity>();
        }

        public virtual void Initialize()
        {
            entities.Clear();
        }

        public void PostInitialize()
        {
            entities.AddRange(newEntities);
            newEntities.Clear();

            for (int i = 0; i < entities.Count; i++)
            {
                var e = entities[i];
                
                if(e is IInitialize)
                    (e as IInitialize).Start();
            }
        }

        public void Update(float deltaS)
        {
            entities.AddRange(newEntities);
            newEntities.Clear();

            for (int i = 0; i < entities.Count; i++)
            {
                var e = entities[i];

                if(e.IsActive())
                    e.Update(deltaS);
                else
                {
                    entities.RemoveAt(i);
                    i--;
                }
            }
        }

        public void Render(float deltaS)
        {
            for (int i = 0; i < entities.Count; i++)
            {
                var e = entities[i];
                e.Render(deltaS);
            }
        }

        public void AddEntity(Entity e)
        {
            newEntities.Add(e);
            e.SetGameState(this);
        }
    }
}
