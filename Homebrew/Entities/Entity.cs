﻿using System.Collections.Generic;
using Homebrew.Entities.Components;
using Homebrew.Entities.Components.Physics;
using Homebrew.GameStates;
using OpenTK;

namespace Homebrew.Entities
{
    public class Entity
    {
        public GameState GameState { get; private set; }
        protected bool Active { get; set; }
        public Vector3 Position { get; set; }
        public Vector3 Rotation { get; set; }
        public Vector3 Scale { get; set; }

        private List<Component> components;

        public Entity()
        {
            components = new List<Component>();
            Active = true;
        }

        public void Update(float deltaS)
        {
            for (int i = 0; i < components.Count; i++)
            {
                var comp = components[i];
                if (comp is IUpdateComponent)
                    (comp as IUpdateComponent).Update(deltaS);
            }
        }

        public void Render(float deltaS)
        {
            for (int i = 0; i < components.Count; i++)
            {
                var comp = components[i];
                if(comp is IRenderComponent)
                    (comp as IRenderComponent).Render(deltaS);
            }
        }

        public void Destroy(Entity e)
        {
            for (int i = 0; i < components.Count; i++)
            {
                var comp = components[i];
                if (comp is IDestroyComponent)
                    (comp as IDestroyComponent).OnDestruction(e);
            }

            Active = false;
        }

        public void OnCollision(Entity e, Contact contact)
        {
            for (int i = 0; i < components.Count; i++)
            {
                var comp = components[i];
                if (comp is IOnCollision)
                    (comp as IOnCollision).OnCollision(e, contact);
            }
        }

        public void HandleCollision(Contact contact)
        {
            for (int i = 0; i < components.Count; i++)
            {
                var comp = components[i];
                if (comp is IHandleCollision)
                    (comp as IHandleCollision).HandleCollision(contact);
            }
        }

        public void AddComponent(Component comp)
        {
            components.Add(comp);
            comp.Owner = this;
        }

        public void SetGameState(GameState state)
        {
            GameState = state;
        }

        public bool IsActive()
        {
            return Active;
        }

        public Matrix4 GetModelviewMatrix()
        {
            var mat = Matrix4.Identity;

            mat *= Matrix4.CreateScale(Scale);
            mat *= Matrix4.CreateRotationX(Rotation.X);
            mat *= Matrix4.CreateRotationY(Rotation.Y);
            mat *= Matrix4.CreateRotationZ(Rotation.Z);
            mat *= Matrix4.CreateTranslation(Position);

            return mat;
        }
    }
}
