﻿namespace Homebrew.Entities.Components
{
    public interface IUpdateComponent
    {
        void Update(float deltaS);
    }
}
