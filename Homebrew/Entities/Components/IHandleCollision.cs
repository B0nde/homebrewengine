﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew.Entities.Components.Physics;

namespace Homebrew.Entities.Components
{
    interface IHandleCollision
    {
        void HandleCollision(Contact contact);
    }
}
