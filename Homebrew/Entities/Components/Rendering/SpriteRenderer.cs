﻿using Homebrew.Rendering;
using Homebrew.Rendering.Shaders;
using Homebrew.Utilities;
using OpenTK;
using OpenTK.Graphics.OpenGL4;

namespace Homebrew.Entities.Components.Rendering
{
    public class SpriteRenderer : Component, IRenderComponent
    {
        private Spritesheet texture;
        private string sprite;
        private Vector4 color;

        public SpriteRenderer(Spritesheet texture, string sprite, Vector4 color)
        {
            this.texture = texture;
            this.sprite = sprite;
            this.color = color;
        }

        public void Render(float deltaS)
        {
            var plane = Mesh.PLANE;

            var effect = StandardEffects.Instance.GetEffect("SpriteDiffuse");
            effect.Bind();

            var tSize = new Vector2(texture.Width, texture.Height);
            var area = texture.Sprites[sprite];

            effect.SetUniform("from", Vector2.Divide(area.Min, tSize));
            effect.SetUniform("to", Vector2.Divide(area.Max, tSize));
            effect.SetUniform("color", color);
            effect.SetUniform("projection", RenderingUtility.Instance.OrthographicProjection);
            effect.SetUniform("modelview", Owner.GetModelviewMatrix());

            texture.Bind();
            plane.Bind();

            GL.DrawArrays(PrimitiveType.Triangles, 0, plane.Size);
        }
    }
}
