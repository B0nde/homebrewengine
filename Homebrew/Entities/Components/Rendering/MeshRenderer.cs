﻿using Homebrew.Rendering;
using Homebrew.Rendering.Shaders;
using OpenTK.Graphics.OpenGL4;

namespace Homebrew.Entities.Components.Rendering
{
    public class MeshRenderer : Component, IRenderComponent
    {
        private Mesh mesh;
        private ShaderProgram effect;
        private Texture texture;

        public MeshRenderer(Mesh mesh, ShaderProgram effect, Texture texture)
        {
            this.mesh = mesh;
            this.effect = effect;
            this.texture = texture;
        }

        public void Render(float deltaS)
        {
            mesh.Bind();
            effect.Bind();
            texture.Bind();
            
            GL.DrawArrays(PrimitiveType.Triangles, 0, mesh.Size);
        }
    }
}
