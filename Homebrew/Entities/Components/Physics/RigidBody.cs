﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew.Utilities;
using OpenTK;

namespace Homebrew.Entities.Components.Physics
{
    public class RigidBody : Component, IUpdateComponent, IHandleCollision
    {
        public Vector3 Velocity { get; set; }

        public RigidBody(Vector3 initialVelocity)
        {
            Velocity = initialVelocity;
        }

        public void Update(float deltaS)
        {
            Velocity += PhysicsUtility.Instance.Gravity;
            Owner.Position += Velocity * deltaS;
        }

        public void HandleCollision(Contact contact)
        {
            if (contact.Normal.X != 0f)
                Velocity = new Vector3(0f, Velocity.Y, Velocity.Z);

            if (contact.Normal.Y != 0f)
                Velocity = new Vector3(Velocity.X, 0f, Velocity.Z);

            if (contact.Normal.Z != 0f)
                Velocity = new Vector3(Velocity.X, Velocity.Y, 0f);

            Owner.Position += contact.Normal * contact.Distance;
        }
    }
}
