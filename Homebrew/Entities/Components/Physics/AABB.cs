﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Homebrew.Entities.Components.Physics
{
    public class AABB : Collider
    {
        public Vector3 Min { get; set; }
        public Vector3 Max { get; set; }

        public AABB(Vector3 min, Vector3 max, Entity owner) : base(owner)
        {
            Max = max;
            Min = min;
        }

        public AABB(Vector3 position, Vector3 size) : base(null)
        {
            var hs = size / 2f;
            Min = position - hs;
            Max = position + hs;
        }

        public AABB(Entity owner) : this(new Vector3(0f, 0f, 0f), new Vector3(0f, 0f, 0f), owner)
        {
        }

        public override void Set(Vector3 position, Vector3 size)
        {
            var hs = size/2f;
            Min = position - hs;
            Max = position + hs;
        }

        public override AABB GetArea()
        {
            return this;
        }
    }
}
