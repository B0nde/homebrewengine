﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Homebrew.Entities.Components.Physics
{
    public class Rectangle : Collider
    {
        public Vector2 Min { get; set; }
        public Vector2 Max { get; set; }

        public Rectangle(Vector2 min, Vector2 max, Entity owner = null) : base(owner)
        {
            Min = min;
            Max = max;
        }

        public Rectangle(Entity owner = null) : this(new Vector2(), new Vector2(), owner)
        {
        }

        public override void Set(Vector3 position, Vector3 size)
        {
            var hs = new Vector2(size.X, size.Y) /2f;
            var pos = new Vector2(position.X, position.Y);
            Min = pos - hs;
            Max = pos + hs;
        }

        public override AABB GetArea()
        {
            return new AABB(new Vector3(Min.X, Min.Y, 0f), new Vector3(Max.X, Max.Y, 0f), Owner);
        }
    }
}
