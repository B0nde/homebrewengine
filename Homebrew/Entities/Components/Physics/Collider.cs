﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Homebrew.Entities.Components.Physics
{
    public abstract class Collider
    {
        public Entity Owner { get; set; }

        public Collider(Entity owner)
        {
            Owner = owner;
        }

        public abstract void Set(Vector3 position, Vector3 size);
        public abstract AABB GetArea();
    }
}
