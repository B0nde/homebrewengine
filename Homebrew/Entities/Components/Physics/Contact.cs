﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace Homebrew.Entities.Components.Physics
{
    public class Contact
    {
        public Vector3 Normal { get; set; }
        public float Distance { get; set; }
        public bool Intersects { get; set; }

        public Contact(Vector3 normal, float distance, bool intersects)
        {
            Distance = distance;
            Normal = normal;
            Intersects = intersects;
        }

        public override string ToString()
        {
            return base.ToString() + "[Normal = " + Normal + ", Distance = "+ Distance + "]";
        }
    }
}
