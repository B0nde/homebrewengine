﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew.Utilities;
using OpenTK;

namespace Homebrew.Entities.Components.Physics
{
    public enum ColliderType { Rectangle, Box }

    public class ColliderComponent : Component, IUpdateComponent
    {
        private Collider collider;
        private Vector3 size;
        private ColliderType type;

        public ColliderComponent(Vector3 size, ColliderType type)
        {
            this.type = type;
            this.size = size;
        }

        public void Update(float deltaS)
        {
            if(collider == null)
                CreateCollider();

            collider.Set(Owner.Position, size);
        }

        private void CreateCollider()
        {
            switch (type)
            {
                case ColliderType.Box:
                    collider = new AABB(Owner);
                    break;

                case ColliderType.Rectangle:
                    collider = new Rectangle(Owner);
                    break;
            }

            PhysicsUtility.Instance.AddCollider(collider);
        }
    }
}
