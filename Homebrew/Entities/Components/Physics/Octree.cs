﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Homebrew.Entities.Components.Physics
{
    internal class Octree
    {
        private enum Node
        {
            UpperLeftFront = 0, UpperRightFront = 1, LowerLeftFront = 2, LowerRightFront = 3,
            UpperLeftBack = 4, UpperRightBack = 5, LowerLeftBack = 6, LowerRightBack = 7, Root = 8
        }

        private const int MAX_LEVEL = 10, MAX_ENTITIES_PER_NODE = 5;

        private Octree[] nodes;
        private List<Collider> colliders;
        private int level;
        private AABB area;

        internal Octree(AABB area, int level = 1)
        {
            this.area = area;
            this.level = level;
            colliders = new List<Collider>();
        }

        internal void Add(Collider c)
        {
            if (nodes != null)
            {
                var node = GetNode(c);
                nodes[(int)node].Add(c);
            }
            else
            {
               colliders.Add(c);
            }

            if (colliders.Count > MAX_ENTITIES_PER_NODE && level < MAX_LEVEL)
                Split();
        }

        internal List<Collider> Get(Collider collider)
        {
            var l = new List<Collider>();

            l.AddRange(colliders);

            if (nodes != null)
            {
                var node = GetNode(collider);
                l.AddRange(nodes[(int) node].Get(collider));
            }

            return l;
        }

        private void Split()
        {
            var hs = (area.Max - area.Min) / 2f;
            var pos = area.Min + hs;

            nodes[(int)Node.LowerLeftBack] = new Octree(new AABB(pos - hs, hs), level + 1);
            nodes[(int)Node.UpperLeftBack] = new Octree(new AABB(new Vector3(pos.X - hs.X, pos.Y + hs.Y, pos.Z - hs.Z), hs), level + 1);
            nodes[(int)Node.LowerRightBack] = new Octree(new AABB(new Vector3(pos.X + hs.X, pos.Y + hs.Y, pos.Z - hs.Z), hs), level + 1);
            nodes[(int)Node.UpperRightBack] = new Octree(new AABB(new Vector3(pos.X + hs.X, pos.Y - hs.Y, pos.Z - hs.Z), hs), level + 1);
            nodes[(int)Node.LowerLeftFront] = new Octree(new AABB(new Vector3(pos.X - hs.X, pos.Y - hs.Y, pos.Z + hs.Z), hs), level + 1);
            nodes[(int)Node.UpperLeftFront] = new Octree(new AABB(new Vector3(pos.X - hs.X, pos.Y + hs.Y, pos.Z + hs.Z), hs), level + 1);
            nodes[(int)Node.LowerRightFront] = new Octree(new AABB(new Vector3(pos.X + hs.X, pos.Y + hs.Y, pos.Z + hs.Z), hs), level + 1);
            nodes[(int)Node.UpperRightFront] = new Octree(new AABB(new Vector3(pos.X + hs.X, pos.Y - hs.Y, pos.Z + hs.Z), hs), level + 1);

            var newColliders = new List<Collider>();

            for (int i = 0; i < colliders.Count; i++)
            {
                var c = colliders[i];
                var node = GetNode(c);
                if(node != Node.Root)
                    nodes[(int)node].Add(c);
                else
                    newColliders.Add(c);
            }

            colliders.Clear();
            colliders.AddRange(newColliders);
            newColliders.Clear();
        }

        internal void Clear()
        {
            if(nodes != null)
                for (int i = 0; i < nodes.Length; i++)
                {
                    nodes[i].Clear();
                    nodes[i] = null;
                }

            nodes = null;
        }

        private Node GetNode(Collider collider)
        {
            var node = Node.Root;
            var cArea = collider.GetArea();
            var areaSize = area.Max - area.Min;
            var hs = areaSize / 2f;
            var med = area.Min + hs;

            var left = cArea.Max.X < med.X;
            var right = cArea.Min.X > med.X;
            var down = cArea.Max.Y > med.Y;
            var up = cArea.Min.Y < med.Y;
            var front = cArea.Min.Z > med.Z;
            var back = cArea.Max.Z < med.Z;

            if (left && down && front)
                node = Node.LowerLeftFront;
            else if (right && down && front)
                node = Node.LowerRightFront;
            else if (left && up && front)
                node = Node.UpperLeftFront;
            else if (right && up && front)
                node = Node.UpperRightFront;
            else if (left && down && back)
                node = Node.LowerLeftBack;
            else if (right && down && back)
                node = Node.LowerRightBack;
            else if (left && up && back)
                node = Node.UpperLeftBack;
            else if (right && up && back)
                node = Node.UpperRightBack;

            return node;
        }
    }
}
