﻿namespace Homebrew.Entities.Components
{
    public interface IRenderComponent
    {
        void Render(float deltaS);
    }
}
