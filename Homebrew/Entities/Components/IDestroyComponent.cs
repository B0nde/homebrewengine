﻿namespace Homebrew.Entities.Components
{
    public interface IDestroyComponent
    {
        void OnDestruction(Entity e);
    }
}
