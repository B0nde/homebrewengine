﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homebrew.Utilities
{
    public enum LoggerLevel { Info, Warning, Error }

    public class Logger
    {
        private static Logger instance;
        public static Logger Instance { get { return instance ?? (instance = new Logger()); } }

        public string LogText { get; private set; }

        private Logger()
        {
            LogText = "";
        }

        public void Log(LoggerLevel loggerLevel, string msg)
        {
            string level = "";

            switch (loggerLevel)
            {
                case LoggerLevel.Info:
                    level = "Info";
                    Console.ForegroundColor = ConsoleColor.White;
                    break;

                case LoggerLevel.Warning:
                    level = "Warning";
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;

                case LoggerLevel.Error:
                    level = "Error";
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
            }

            var line = "[" + DateTime.Now + "][" + level + "] " + msg;
            LogText += line;
            Console.WriteLine(line);

            if (loggerLevel == LoggerLevel.Error || loggerLevel == LoggerLevel.Warning)
                DumpToFile("./log.txt");
        }

        public void DumpToFile(string filename)
        {
            using (var writer = new StreamWriter(filename))
            {
                writer.WriteLine(LogText);
            }
        }
    }
}
