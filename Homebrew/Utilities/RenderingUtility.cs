﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;
using OpenTK.Graphics.OpenGL4;

namespace Homebrew.Utilities
{
    public class RenderingUtility
    {
        private static RenderingUtility instance;
        public static RenderingUtility Instance { get { return instance ?? (instance = new RenderingUtility()); } }

        public Matrix4 OrthographicProjection { get; private set; }
        public Matrix4 PerspectiveProjection { get; private set; }
        public float ViewingDistance { get; set; }

        private RenderingUtility()
        {
            ViewingDistance = 1000f;
        }

        public void InitializeRendering(int width, int height, float fov = 0.785398163f)
        {
            SetPerspectiveProjection(width, height, fov, ViewingDistance);
            SetProjectionOrthographic(width, height);

            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

            GL.Enable(EnableCap.CullFace);
            GL.CullFace(CullFaceMode.Back);
            GL.FrontFace(FrontFaceDirection.Cw);

            GL.Enable(EnableCap.DepthTest);
            GL.Viewport(0, 0, width, height);
        }

        public void SetProjectionOrthographic(float width, float height)
        {
            OrthographicProjection = Matrix4.CreateOrthographicOffCenter(0f, width, 0f, height, 0.000001f, ViewingDistance);
        }

        public void SetPerspectiveProjection(float width, float height, float fov, float zFar)
        {
            PerspectiveProjection = Matrix4.CreatePerspectiveFieldOfView(fov, width / height, 0.00001f, zFar);
        }
    }
}
