﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using Homebrew.Entities.Components.Physics;
using OpenTK;

namespace Homebrew.Utilities
{
    public class PhysicsUtility
    {
        private static PhysicsUtility instance;
        public static PhysicsUtility Instance { get { return instance ?? (instance = new PhysicsUtility()); } }

        public Vector3 Gravity { get; set; }

        private List<Collider> colliders;

        private PhysicsUtility()
        {
            colliders = new List<Collider>();
            Gravity = new Vector3(0f, -10f, 0f);
        }

        internal void UpdatePhysics()
        {
            // TODO: Make colliders more generic

            if (colliders.Count > 0)
            {
                // Fill collision tree
                var collisionTree = new Octree(new AABB(new Vector3(-1000f, -1000f, -1000f), new Vector3(2000f, 2000f, 2000f)));

                foreach (var e in colliders)
                {
                    collisionTree.Add(e);
                }

                for (int i = 0; i < colliders.Count; i++)
                {
                    var e = colliders[i];

                    foreach (var ee in collisionTree.Get(e))
                    {
                        if (!e.Equals(ee) && ((e is AABB && ee is AABB) || e is Rectangle && ee is Rectangle))
                        {
                            var contact = Intersects(e, ee);

                            if (contact.Intersects)
                            {
                                e.Owner.HandleCollision(contact);
                                e.Owner.OnCollision(ee.Owner, contact);

                                contact.Normal = -contact.Normal;

                                ee.Owner.OnCollision(e.Owner, contact);
                            }
                        }
                    }
                }
            }
        }

        public void AddCollider(Collider collider)
        {
            colliders.Add(collider);
            //Console.WriteLine(collider);
        }

        public void RemoveCollider(Collider collider)
        {
            colliders.Remove(collider);
        }

        private static Contact Intersects(Collider collider1, Collider collider2)
        {
            if (collider1 is Rectangle && collider2 is Rectangle)
                return Intersects(collider1 as Rectangle, collider2 as Rectangle);

            if (collider1 is AABB && collider2 is AABB)
                return Intersects(collider1 as AABB, collider2 as AABB);

            throw new Exception("Collider not implemented!");
            return null;
        }

        private static Contact Intersects(AABB aabb1, AABB aabb2)
        {
            var contact = new Contact(new Vector3(0f, 0f, 0f), float.MaxValue, false);

            if (!TestAxis(Vector3.UnitX, aabb1.Min.X, aabb1.Max.X, aabb2.Min.X, aabb2.Max.X, ref contact))
                return contact;

            if (!TestAxis(Vector3.UnitY, aabb1.Min.Y, aabb1.Max.Y, aabb2.Min.Y, aabb2.Max.Y, ref contact))
                return contact;

            if (!TestAxis(Vector3.UnitZ, aabb1.Min.Z, aabb1.Max.Z, aabb2.Min.Z, aabb2.Max.Z, ref contact))
                return contact;

            contact.Distance = (float) Math.Sqrt(contact.Distance) * 1.000001f;
            contact.Normal = contact.Normal.Normalized();
            contact.Intersects = true;

            return contact;
        }

        private static Contact Intersects(Rectangle r1, Rectangle r2)
        {
            var contact = new Contact(new Vector3(0f, 0f, 0f), float.MaxValue, false);

            if (!TestAxis(Vector3.UnitX, r1.Min.X, r1.Max.X, r2.Min.X, r2.Max.X, ref contact))
                return contact;

            if (!TestAxis(Vector3.UnitY, r1.Min.Y, r1.Max.Y, r2.Min.Y, r2.Max.Y, ref contact))
                return contact;

            contact.Distance = (float)Math.Sqrt(contact.Distance) * 1.000001f;
            contact.Normal = contact.Normal.Normalized();
            contact.Intersects = true;

            return contact;
        }

        private static bool TestAxis(Vector3 axis, float minA, float maxA, float minB, float maxB, ref Contact contact)
        {
            var axisLength = axis.LengthSquared;

            if (axisLength < 1.0e-8f)
                return true;

            var d0 = maxB - minA;
            var d1 = maxA - minB;

            if (d0 <= 0f || d1 <= 0f)
                return false;

            var d = d0 < d1 ? d0 : -d1;
            var sep = axis * (d / axisLength);
            var sepLength = sep.LengthSquared;

            if (sepLength < contact.Distance)
            {
                contact.Distance = sepLength;
                contact.Normal = sep;
            }

            return true;
        }
    }
}
