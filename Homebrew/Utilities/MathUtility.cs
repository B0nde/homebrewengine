﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homebrew.Utilities
{
    public class MathUtility
    {
        public static float Clamp(float value, float min, float max)
        {
            var v = value;

            if (v > max) v = max;
            else if (v < min) v = min;

            return v;
        }

        public static float DegreeToRadians(float degrees)
        {
            return degrees * 0.0174532925f;
        }
    }
}
