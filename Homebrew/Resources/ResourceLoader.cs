﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew.Rendering;
using Homebrew.Utilities;
using Rectangle = Homebrew.Entities.Components.Physics.Rectangle;
using OpenTK;

namespace Homebrew.Resources
{
    public class ResourceLoader
    {
        private static ResourceLoader instance;
        public static ResourceLoader Instance { get { return instance ?? (instance = new ResourceLoader()); } }

        private const string RESOURCE_LOCATION = "./Resources/";
        private const string IMAGE_LOCATION = RESOURCE_LOCATION + "Images/";
        private const string SPRITESHEET_LOCATION = IMAGE_LOCATION + "Spritesheets/";

        private Dictionary<string, Texture> textures; 

        private ResourceLoader()
        {
            textures = new Dictionary<string, Texture>();
        }

        public Texture GetTexture(string name)
        {
            if (textures.ContainsKey(name))
                return textures[name];

            var bitmap = new Bitmap(IMAGE_LOCATION + name + ".png");
            var texture = new Texture(bitmap);
            textures.Add(name, texture);

            return texture;
        }

        public Spritesheet GetSpritesheet(string name)
        {
            if (textures.ContainsKey(name))
                return textures[name] as Spritesheet;

            var spritesheet = LoadSpritesheet(name);
            textures.Add(name, spritesheet);
            return spritesheet;
        }

        private Spritesheet LoadSpritesheet(string name)
        {
            var bitmap = new Bitmap(SPRITESHEET_LOCATION + name + ".png");
            var spritesheet = new Spritesheet(bitmap);

            var filename = SPRITESHEET_LOCATION + name + ".sprites";

            using (var reader = new StreamReader(new FileStream(filename, FileMode.Open)))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine().Trim();
                    var splits = line.Split(',');
                    
                    if(splits.Length != 5)
                        Logger.Instance.Log(LoggerLevel.Error, "Not enough or too many arguments in sprites file at line: " + line + " in file: " + filename);

                    var spriteName = splits[0].Trim();
                    int fromX, fromY, toX, toY;

                    if(!int.TryParse(splits[1].Trim(), out fromX))
                        Logger.Instance.Log(LoggerLevel.Error, "Error in sprites file: " + filename + " at line: " + line);

                    if (!int.TryParse(splits[2].Trim(), out fromY))
                        Logger.Instance.Log(LoggerLevel.Error, "Error in sprites file: " + filename + " at line: " + line);

                    if (!int.TryParse(splits[3].Trim(), out toX))
                        Logger.Instance.Log(LoggerLevel.Error, "Error in sprites file: " + filename + " at line: " + line);

                    if (!int.TryParse(splits[4].Trim(), out toY))
                        Logger.Instance.Log(LoggerLevel.Error, "Error in sprites file: " + filename + " at line: " + line);
                    
                    spritesheet.Sprites.Add(spriteName, new Rectangle(new Vector2(fromX, fromY), new Vector2(toX, toY)));
                }
            }

            return spritesheet;
        }
    }
}
