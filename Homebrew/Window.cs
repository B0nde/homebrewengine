﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew.GameStates;
using Homebrew.Utilities;
using OpenTK;
using OpenTK.Graphics.OpenGL4;

namespace Homebrew
{
    public class Window : GameWindow
    {
        private GameState state;
        private int lastUpdate = DateTime.Now.Millisecond;
        private int lastRender = DateTime.Now.Millisecond;

        public Window(int width, int height, string title, GameState state, bool fullscreen = false)
        {
            Width = width;
            Height = height;
            Title = title;
            WindowBorder = WindowBorder.Fixed;

            this.state = state;
        }

        protected override void OnLoad(EventArgs e)
        {
            Logger.Instance.Log(LoggerLevel.Info, "Loading...");
            var startTime = DateTime.Now.Millisecond;
            base.OnLoad(e);
            RenderingUtility.Instance.InitializeRendering(Width, Height);
            state.Initialize();
            state.PostInitialize();
            Logger.Instance.Log(LoggerLevel.Info, "Loading finished in " + (DateTime.Now.Millisecond - startTime) + "ms.");
        }

        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            base.OnUpdateFrame(e);

            var nowTime = DateTime.Now.Millisecond;
            var delta = nowTime - lastUpdate;
            var deltaS = delta/1000f;
            lastUpdate = nowTime;
            deltaS = MathUtility.Clamp(deltaS, 0.00000000001f, 10f);

            state.Update(deltaS);
            PhysicsUtility.Instance.UpdatePhysics();
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            base.OnRenderFrame(e);

            var nowTime = DateTime.Now.Millisecond;
            var delta = nowTime - lastRender;
            lastRender = nowTime;

            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            state.Render(delta / 1000f);

            SwapBuffers();
        }

        public void ChangeGameState(GameState state)
        {
            this.state = state;
            this.state.Initialize();
        }
    }
}
