﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Homebrew;

namespace Hemobrew.Medival
{
    class Program
    {
        static void Main(string[] args)
        {
            var window = new Window(800, 600, "Medival", new MedivalGameState());
            window.Run();
        }
    }
}
